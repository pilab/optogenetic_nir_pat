% This program is provided as a demo to process the photoswitching images
% obtained by photoacoustic computed tomography of BphP1-expressing cells
% or animals. 

% This program shoud be used with the demo data 'Demo_PS_4T1.mat'.
% The demo data contains 10 switching cycles of a scattering phantom made
% of BphP1-expressing 4T1 cells embedded in intralip-argar 
% This program was tested on Matlab R2019b
% This program was orignally developed by Duke Photoacoustic Imaging Lab

% More information can be provided upon request
% Contact: Junjie Yao, Duke Univeisty
% Email: junjie.yao@duke.edu

clear all;
clc;
close all;
%% Load the demo data
load Demo_PS_4T1_final.mat
pa_raw = pa_raw/max(abs(pa_raw(:))); % Normalize the raw PA data

%% Pre-prcess the raw PA data
dim = size(pa_raw);
p = zeros(1,dim(3)); % Averaged signal intensity
pa_hilbert = 0*pa_raw;% Apply Hilbert transform to the raw PA data 
figure(1);
for i = 1:dim(3)
    
    tmp = pa_raw(:,:,i);
    pa_hilbert(:,:,i) = abs(hilbert(tmp));% Take the hilbert transform
    tmp2 = pa_hilbert(130:170,130:170,i);
    p(i) = mean(mean(tmp2));
    subplot(1,2,1)
    imshow(pa_hilbert(:,:,i),[0 1]);
    title(strcat('Frame #',num2str(i),';',{' '},'Cycle #',num2str(ceil(i/st_duration))));
    subplot(1,2,2);
    plot([1:i],p(1:i)/p(1));
    xlabel('Frame #');
    ylabel('PA signal');
    axis square;
    drawnow;
end
%% Process the photoswitching data
figure(2);
pa_switching = zeros(dim(1),dim(2),st_cycles);
for i = 1:st_cycles
    
    pa_ON = mean(pa_hilbert(:,:,(i-1)*st_duration+[1:2]),3); % ON-stage PA image
    pa_OFF = mean(pa_hilbert(:,:,(i-1)*st_duration+[st_duration-5:st_duration]),3); % ON-stage PA image
    pa_switching(:,:,i) = pa_ON - pa_OFF; % Differntial PA image
    imshow(pa_switching(:,:,i),[0 1]);
    title(strcat('Diff. image:',{' '},'Cycle #',num2str(i)));
    drawnow;
end
%% Compare the raw PA data and the photoswitching PA data
figure(3);
subplot(1,2,1);
imshow(mean(pa_hilbert,3),[0 0.2]);
title('Averaged raw PA image');
subplot(1,2,2);
imshow(mean(pa_switching,3),[0 1]);
title('Averaged ps PA image');