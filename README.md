# Photoacoustic imaging of a near-infrared transgenic phytochrome
**Description:** Photoacoustic tomography (PAT) of genetically encoded probes allows for imaging of targeted biological processes deep in tissues with high spatial resolution; however, high background signals from blood can limit the achievable detection sensitivity. Here we have developed a reversibly-switchable nonfluorescent bacterial phytochrome for use in photoacoustic imaging, BphP1, with the most red-shifted absorption among genetically encoded probes. BphP1 binds a heme-derived biliverdin chromophore and is reversibly photoconvertible between red and near-infrared light-absorption states. We combined single-wavelength PAT with efficient BphP1 photoswitching, which enabled differential imaging with substantially decreased background signals, enhanced detection sensitivity, increased penetration depth and improved spatial resolution. The presented demo data and code are used for demonstrating the basic image processing in photoswitching photoacoustic tomography.

# Repo Files
1. `Demo_PS_4T1_final.mat` - Demo data for photoswitching photoacoustic imaging
+ The data was acquired on the BphP1-expressing 4T1 cells mixed with argar/intralipid phantom 
+ The data was acquired by ring-shaped-array PAT system
+ The raw data is saved in matrix *pa_raw*
+ pa_raw's three dimension: 1st dimension, x-axis, pixel size, 0.317 **mm**; 2nd dimension, y-axis, pixel size, 0.317 **mm**; 3rd  
dimension, frame number, 0.2 seconds per frame

2. `Photoswitching_PAT_data_processing_demo.m` - Demo Matlab code
+ The program is self running with all the sections annotated
+ The program was tested on `Matlab R2019b`
+ The figure generation procedure should be self explanatory from the code
